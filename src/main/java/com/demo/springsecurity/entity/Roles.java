package com.demo.springsecurity.entity;

import javax.persistence.*;

@Entity
@Table
public class Roles {
    @Id
    @GeneratedValue
    @Column(name = "role_id")
    private long roleId;
    private String role;

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
