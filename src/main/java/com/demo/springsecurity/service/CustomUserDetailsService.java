package com.demo.springsecurity.service;

import com.demo.springsecurity.entity.Users;
import com.demo.springsecurity.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        Users users= userRepository.findByUsername(s);
        if(users==null){
            throw new UsernameNotFoundException("User not found...");
        }
        return new CustomUserDetails(users);

    }
}
